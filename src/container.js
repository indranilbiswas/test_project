import React,{useState} from 'react'
import RedRectangle from './redRectangle';
const Container = ()=>{
    const initialState = ''
    const [text, setText] = useState(initialState)
    const changeColor = ()=>{
        let text = "Button clicked"
        const tarElm = document.querySelector('.red_rec')
        tarElm.classList.toggle('green_rec_color')
        setText(text)
    }
    return(<>
    <RedRectangle />
    <button className="red_btn" onClick={changeColor}>Press</button>
    <p id="showText">{text}</p>
    </>)
}

export default Container;